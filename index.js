import express from "express";

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

import pokemonRoute from "./Routes/pokemonRoute.js";

app.use("/api/pokemon", pokemonRoute);

app.all("*", (req, res) => {
  res.status(404).json({
    message: "Api not found",
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
