import axios from "axios";
import fs from "fs";

const baseUrl = "https://pokeapi.co/api/v2/pokemon/";
const baseUrlLimit = "https://pokeapi.co/api/v2/pokemon/?limit=100";

import isPrime from "../Helper/primeCheck.js";
import generateFibonacci from "../Helper/fibonnaciNum.js";

const dataBase = JSON.parse(fs.readFileSync("./Database/db.json"));
const dbPokemon = dataBase.myPokemonList;

const pokemonController = {
  getAllPokemon: async (req, res) => {
    try {
      const getData = await axios.get(baseUrlLimit);

      const pokemonData = getData.data.results;
      const pokemonName = pokemonData.map((nama) => nama.name);

      res.status(200).json({
        message: "List all pokemon name",
        data: pokemonName,
      });
    } catch (error) {
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
  getPokemonDetail: async (req, res) => {
    try {
      const pokemonName = req.params.name;
      const getData = await axios.get(baseUrl + pokemonName);

      const pokemonData = getData.data;

      res.status(200).json({
        message: `Detail pokemon ${req.params.name}`,
        data: pokemonData,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
  catchPokemon: async (req, res) => {
    try {
      const getData = await axios.get(baseUrlLimit);
      const getName = getData.data.results.map((pokemon) => pokemon.name);
      const newId = dbPokemon[dbPokemon.length - 1].id + 1;

      const isSuccess = Math.random() < 0.5;

      if (isSuccess) {
        const randNum = Math.floor(Math.random() * 100);
        dbPokemon.push({
          id: newId,
          name: getName[randNum],
          release: 0,
        });

        fs.writeFileSync(
          "./Database/db.json",
          JSON.stringify({ myPokemonList: dbPokemon })
        );

        res.status(200).json({
          success: true,
          message: `You caught ${getName[randNum]}!`,
        });
      } else {
        res.json({
          message: "Failed to catch a Pokemon.",
        });
      }
    } catch (error) {
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
  getMyPokemonData: (req, res) => {
    try {
      res.status(200).json({
        message: "This is your pokemon",
        data: dbPokemon,
      });
    } catch (error) {
      res.status(500).json({
        message: "Internal server Error",
      });
    }
  },
  pokemonRelease: (req, res) => {
    try {
      const randNum = Math.floor(Math.random() * 100);

      if (isPrime(randNum)) {
        res.status(200).json({
          message: `Released Pokemon successfully! ${randNum} is a prime number.`,
        });
      } else {
        res.json({
          message: `Release failed. ${randNum} is not a prime number.`,
        });
      }
    } catch (error) {
      res.status(500).json({
        message: "Internal server error",
      });
    }
  },
  renamePokemon: (req, res) => {
    try {
      const renamePokemon = (name, release) => {
        const splitName = name.split("-");
        const originalName = splitName[0];
        const fiboKey = generateFibonacci(release);
        return originalName + "-" + fiboKey;
      };

      const selectedPokemon = dbPokemon.find(
        (item) => item.id == req.params.id
      );

      if (selectedPokemon) {
        const newPokemon = dbPokemon.map((item) => {
          if (item.id == req.params.id) {
            item.name = renamePokemon(item.name, item.release);
            item.release += 1;
          }
          return item;
        });

        fs.writeFileSync(
          "./Database/db.json",
          JSON.stringify({ myPokemonList: newPokemon })
        );

        res.json({
          message: `Renamed Pokemon successfully to ${selectedPokemon.name}!`,
          success: true,
          data: selectedPokemon,
        });
      } else {
        res.json({
          message: `Rename failed. Pokemon not found.`,
          success: false,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        message: "Internal server error",
      });
    }
  },
};

export default pokemonController;
