import express from "express";
import pokemonController from "../Controller/pokemonController.js";

const router = express.Router();
router.get("/", pokemonController.getAllPokemon);
router.get("/detail/:name", pokemonController.getPokemonDetail);
router.get("/catch", pokemonController.catchPokemon);
router.get("/mine", pokemonController.getMyPokemonData);
router.get("/release", pokemonController.pokemonRelease);
router.get("/rename/:id", pokemonController.renamePokemon);

export default router;
