import { expect } from "chai";
import data from "./Controller/pokemonController.js";
import sinon from "sinon";

describe("pokemon controller", () => {
  describe("getAllPokemon", () => {
    it("should retrieve and return a list of Pokemon names", async () => {
      const req = {};
      const res = {
        status: function (statusCode) {
          expect(statusCode).to.equal(200);
          return this;
        },
        json: function (data) {
          expect(data).to.have.property("message", "List all pokemon name");
          expect(data).to.have.property("data").that.is.an("array");
        },
      };

      await data.getAllPokemon(req, res);
    });
  });
  describe("getDetailPokemon", () => {
    it("should retrieve and return a detail of Pokemon", async () => {
      const req = {
        params: {
          name: "dugtrio",
        },
      };
      const res = {
        status: function (statusCode) {
          expect(statusCode).to.equal(200);
          return this;
        },
        json: function (data) {
          expect(data).to.have.property("message", "Detail pokemon dugtrio");
          expect(data).to.have.property("data").that.is.an("object");
        },
      };

      await data.getPokemonDetail(req, res);
    });
  });
  describe("catchPokemon", () => {
    it("should catch the pokemon", async () => {
      const randomStub = sinon.stub(Math, "random").returns(0.25);
      const req = "";
      const res = {
        status: function (statusCode) {
          expect(statusCode).to.equal(200);
          return this;
        },
        json: function (data) {
          expect(data).to.have.property("success", true);
          expect(data).to.have.property("message").that.is.an("string");
        },
      };

      await data.catchPokemon(req, res);
      randomStub.restore();
    });
    it("failure catch pokemon", async () => {
      const randomStub = sinon.stub(Math, "random").returns(0.75); // Simulate failure
      const req = "";
      const res = {
        status: function (statusCode) {
          expect(statusCode).to.equal(500);
          return this;
        },
        json: function (data) {
          expect(data).to.have.property("message").that.is.an("string");
        },
      };

      await data.catchPokemon(req, res);

      randomStub.restore();
    });
  });
  describe("getMyPokemon", () => {
    it("should retrieve and return my Pokemon list", async () => {
      const req = {};
      const res = {
        status: function (statusCode) {
          expect(statusCode).to.equal(200);
          return this;
        },
        json: function (data) {
          expect(data).to.have.property("message", "This is your pokemon");
          expect(data).to.have.property("data").that.is.an("array");
        },
      };

      data.getMyPokemonData(req, res);
    });
  });
});
